# Exchange Mailer

Simple commandline mailer script wrapping 
[MS Office Exchange Web Services 
API](https://github.com/OfficeDev/ews-java-api).


# Prereqs

Requires Groovy to run. Tested with 2.4.3. 

Groovy requires Java to run. :-)


# Usage
Clone the repository, or manually download the following file and run: 

        groovy ExchangeMailer.groovy

Will show you options and help. First invocation may be quite slow as it pulls 
down libraries via Groovy's Grapes mechanism.


# Usage Example and Tips

The mailer has an annoying, but necessary, number of required options to 
specify by hand, my approach is to write a small shell script supplying all the 
common parameters, instead of calling this script directly.

Also a good idea to set-up your environment with environment variables 
containing various common parameters. With enough of these, commandline use is 
very convenient.

For example, I have the following snippet:

        MAILER=${HOME}/checkouts/exchange-mailer/ExchangeMailer.groovy

        groovy "${MAILER}" \
          -u "${EXCHANGE_WS_URL}" \
          -c "${CREDENTIALS_FILE}" \
          -f "${FROM_ME}" \
          -r "${RECIPIENT1}" "${RECIPIENT2}" \
          -s "${subject}" \
          -b "${message}"


Note also the `-p message-body-file.txt` option if you have a longer, possibly 
preprocessed, message template.


# Scope and Restrictions

## Messaging

Only supports sending plain text messages, more specifically, no setting of 
mime-type -- so you can send HTML text, for example, it will just not be 
interpreted on the receiving end so well! Though it may... haven't tested.

Attachments are allowed.

## Autodiscovery

Auto-discovery is not implemented, mainly because I don't have a working 
exchange environment to test this feature.

## Attachments and Memory Use

Attachments are loaded into memory as part of message creation, and thus very 
large attachments may cause an out of memory error. Then again, you probably 
should not be sending very large attachments via email.

## Credentials Passing

This is done by specifying a credentials file of the format:
        
        <username on line 1>
        <password on line 2>
        [optional trailing empty line in file, max 3 lines in total]
        

I have deliberately not allowed direct options to specify username and 
password, even if this seems convenient, for the following reasons:

* I take inspiration from SSH key files, in most cases, my passwords are 
generated and have all sorts of whacky characters that typing them out is 
simply not feasible.
* Specifying credentials on the commandline makes it visible on running `ps`.
* Putting the credentials on the file, you can always `chmod` the file 
appropriately, or even encrypt it, and decrypt as needed.
* It's one less option to worry about. Of course, the complexity is shifted to 
interpreting the credentials file, so this is isn't a very good reason. :-)

