@GrabResolver(name='Sonatype Open Source Repo', root='https://oss.sonatype.org/content/repositories/snapshots/')
@Grapes([
  @Grab('org.codehaus.groovy:groovy-all:2.4.3'),
  @Grab('com.microsoft.ews-java-api:ews-java-api:3.0-SNAPSHOT')
])

import groovy.transform.SourceURI

import org.apache.commons.cli.Option

import microsoft.exchange.webservices.data.core.ExchangeService
import microsoft.exchange.webservices.data.core.service.item.EmailMessage
import microsoft.exchange.webservices.data.credential.WebCredentials
import microsoft.exchange.webservices.data.credential.ExchangeCredentials
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion
import microsoft.exchange.webservices.data.property.complex.MessageBody
import microsoft.exchange.webservices.data.property.complex.EmailAddress


@SourceURI
def scriptUri

String header = """$scriptUri.path <options>

Sends an email via the MS Exchange Web Services API.

"""
String footer = "Both subject and message body are optional, and an empty email will be sent if unspecified."

def cli = new CliBuilder(header: header, footer: footer)
cli.with {
  u longOpt: 'url', '[Required] Exchange Web Service URL, e.g. https://webmail.your-domain.com/ews/Exchange.asmx',
    required: true, args: 1, argName: 'Exchange Web Service URL'
  c longOpt: 'credentialsFilePath', '[Required] path to file containing auth credentials, username on first line, password on next line',
    required: true, args: 1, argName: 'credentials file'
  f longOpt: 'from', '[Required] Sender Email Address, e.g. you@your-exchange-domain.com',
    required: true, args: 1, argName: 'Sender Email Address'
  r longOpt: 'recipient', '[Required] Recipient Email Addresses, at least one required, space-separated, e.g. foo@doop.com bar@doop.com',
    required: true, args: Option.UNLIMITED_VALUES, argName: 'Recipient Email Addresses', valueSeparator: ' '
  s longOpt: 'subject', '[Optional] Email Subject',
    required: false, args: 1, argName: 'Email Subject'
  b longOpt: 'body', '[Optional] Message contents, takes priority over --bodyFilePath.',
    required: false, args: 1, argName: 'Message Body'
  p longOpt: 'bodyFilePath', '[Optional] File containing message body, alternative to --body. Reads text using default system charset.',
    required: false, args: 1, argName: 'Message Body File'
  a longOpt: 'attachment', '[Optional] Paths to files to attach, space-separated. Base name of each path will be used as the attachment name, e.g. image1.jpg image2.jpg',
    required: false, args: Option.UNLIMITED_VALUES, argName: 'Attachment File Paths', valueSeparator: ' '
}

def options = cli.parse(args)

if (!options) {
  System.exit(0)
}

// Util.printOptions(options)

def creds = Util.getCredentials(options.credentialsFilePath)

new Service(
  url: options.url,
  username: creds.username,
  password: creds.password
)
.init()
.send(
  new Message(
    from: options.from,
    recipients: Util.cleanSpaceSeparatedOptArgs(options.recipients),
    subject: options.subject ?: '',
    body: options.body ? options.body : (options.bodyFilePath ? Util.readMessageFile(options.bodyFilePath) : ''),
    attachments: Util.cleanSpaceSeparatedOptArgs(options.attachments)
  )
)

System.exit(0)


class Message {
  String from
  Set<String> recipients
  String subject
  String body
  List<String> attachments
}


class Service {
  String url
  String username
  String password

  private ExchangeService service

  Service init() {
    service = new ExchangeService(
      url: new URI(url),
      credentials: new WebCredentials(username, password)
    )
    // while auto-discover is recommended, many places have autodiscover badly
    // set-up, for this use case, just hitting the URL directly.

    this
  }

  void send(Message msg) {
    assert service

    EmailMessage emailMsg = new EmailMessage(service)
    emailMsg.from = new EmailAddress(msg.from)
    emailMsg.subject = msg.subject ?: ''
    emailMsg.body = MessageBody.getMessageBodyFromText(msg.body)
    msg.recipients.each {
      emailMsg.toRecipients.add(it)
    }
    msg.attachments.each {
      def (name, contents) = Util.readAttachment(it)
      emailMsg.attachments.addFileAttachment(name, contents)
    }

    emailMsg.send()
  }
}

class Util {
  static void printOptions(options) {
    def cast = { arg, clazz -> arg ? clazz.cast(arg) : arg }
    def castStr = { arg -> cast(arg, String) }

    println([
      url: options.url,
      credentialsFilePath: castStr(options.credentialsFilePath),
      sender: castStr(options.from),
      recipients: cast(options.recipients, List),
      subject: castStr(options.subject),
      body: castStr(options.body),
      bodyFilePath: castStr(options.bodyFilePath),
      attachments: cast(options.attachments, List)
    ])
  }

  static List<String> cleanSpaceSeparatedOptArgs(spaceSepOptArgs) {
    // CliBuilder oddity, adds a redundant "--" when using space separator
    spaceSepOptArgs ?
      (spaceSepOptArgs as List<String>).findAll { it != '--' } :
      []
  }

  static Map<String, String> getCredentials(String credentialsFilePath) {
    def lines = new FileInputStream(toFile(credentialsFilePath)).readLines()
    assert lines.size() in [2, 3], "Credentials File: Expecting at most 3 lines, only first 2 lines will be taken for username and password"

    // println("CREDS: ${lines[0]}, ${lines[1]}")
    [username: lines[0], password: lines[1]]
  }

  static List readAttachment(String attachmentFilePath) {
    File f = toFile(attachmentFilePath)
    [f.name, f.bytes]
  }

  static String readMessageFile(String messageFilePath) {
    toFile(messageFilePath).text
  }

  private static File toFile(String filePath) {
    File f = new File(filePath)
    assert f.exists()
    assert f.canRead()
    assert f.isFile()

    f
  }

}
